/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package POJOS;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gizem
 */
@Entity
@Table(name = "kullanici")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kullanici.findAll", query = "SELECT k FROM Kullanici k"),
    @NamedQuery(name = "Kullanici.findByKullaniciAdi", query = "SELECT k FROM Kullanici k WHERE k.kullaniciAdi = :kullaniciAdi"),
    @NamedQuery(name = "Kullanici.findByEposta", query = "SELECT k FROM Kullanici k WHERE k.eposta = :eposta"),
    @NamedQuery(name = "Kullanici.findByAd", query = "SELECT k FROM Kullanici k WHERE k.ad = :ad"),
    @NamedQuery(name = "Kullanici.findBySoyad", query = "SELECT k FROM Kullanici k WHERE k.soyad = :soyad"),
    @NamedQuery(name = "Kullanici.findBySifre", query = "SELECT k FROM Kullanici k WHERE k.sifre = :sifre"),
    @NamedQuery(name = "Kullanici.findByGizliSoru", query = "SELECT k FROM Kullanici k WHERE k.gizliSoru = :gizliSoru"),
    @NamedQuery(name = "Kullanici.findByGizliCevap", query = "SELECT k FROM Kullanici k WHERE k.gizliCevap = :gizliCevap"),
    @NamedQuery(name = "Kullanici.findByTelefon", query = "SELECT k FROM Kullanici k WHERE k.telefon = :telefon"),
    @NamedQuery(name = "Kullanici.findByCinsiyet", query = "SELECT k FROM Kullanici k WHERE k.cinsiyet = :cinsiyet")})
public class Kullanici implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kullanici_adi")
    private String kullaniciAdi;
    @Column(name = "eposta")
    private String eposta;
    @Basic(optional = false)
    @Column(name = "ad")
    private String ad;
    @Basic(optional = false)
    @Column(name = "soyad")
    private String soyad;
    @Basic(optional = false)
    @Column(name = "sifre")
    private String sifre;
    @Column(name = "gizli_soru")
    private String gizliSoru;
    @Column(name = "gizli_cevap")
    private String gizliCevap;
    @Column(name = "telefon")
    private String telefon;
    @Column(name = "cinsiyet")
    private String cinsiyet;
    @JoinColumn(name = "egitimID", referencedColumnName = "egitimID")
    @ManyToOne
    private Egitim egitimID;
    @JoinColumn(name = "isID", referencedColumnName = "isID")
    @ManyToOne
    private IsBilgileri isID;

    public Kullanici() {
    }

    public Kullanici(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }

    public Kullanici(String kullaniciAdi, String ad, String soyad, String sifre) {
        this.kullaniciAdi = kullaniciAdi;
        this.ad = ad;
        this.soyad = soyad;
        this.sifre = sifre;
    }

    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        this.kullaniciAdi = kullaniciAdi;
    }

    public String getEposta() {
        return eposta;
    }

    public void setEposta(String eposta) {
        this.eposta = eposta;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public String getGizliSoru() {
        return gizliSoru;
    }

    public void setGizliSoru(String gizliSoru) {
        this.gizliSoru = gizliSoru;
    }

    public String getGizliCevap() {
        return gizliCevap;
    }

    public void setGizliCevap(String gizliCevap) {
        this.gizliCevap = gizliCevap;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getCinsiyet() {
        return cinsiyet;
    }

    public void setCinsiyet(String cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public Egitim getEgitimID() {
        return egitimID;
    }

    public void setEgitimID(Egitim egitimID) {
        this.egitimID = egitimID;
    }

    public IsBilgileri getIsID() {
        return isID;
    }

    public void setIsID(IsBilgileri isID) {
        this.isID = isID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kullaniciAdi != null ? kullaniciAdi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kullanici)) {
            return false;
        }
        Kullanici other = (Kullanici) object;
        if ((this.kullaniciAdi == null && other.kullaniciAdi != null) || (this.kullaniciAdi != null && !this.kullaniciAdi.equals(other.kullaniciAdi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "POJOS.Kullanici[ kullaniciAdi=" + kullaniciAdi + " ]";
    }
    
}
