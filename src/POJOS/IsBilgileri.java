/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package POJOS;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gizem
 */
@Entity
@Table(name = "is_bilgileri")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IsBilgileri.findAll", query = "SELECT \u0131 FROM IsBilgileri \u0131"),
    @NamedQuery(name = "IsBilgileri.findByIsID", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.isID = :isID"),
    @NamedQuery(name = "IsBilgileri.findByCalismaDurumu", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.calismaDurumu = :calismaDurumu"),
    @NamedQuery(name = "IsBilgileri.findByFirmaAdi", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.firmaAdi = :firmaAdi"),
    @NamedQuery(name = "IsBilgileri.findByBaslangicTarihi", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.baslangicTarihi = :baslangicTarihi"),
    @NamedQuery(name = "IsBilgileri.findByBitisTarihi", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.bitisTarihi = :bitisTarihi"),
    @NamedQuery(name = "IsBilgileri.findBySektor", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.sektor = :sektor"),
    @NamedQuery(name = "IsBilgileri.findByUlke", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.ulke = :ulke"),
    @NamedQuery(name = "IsBilgileri.findByIsAlani", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.isAlani = :isAlani"),
    @NamedQuery(name = "IsBilgileri.findByPozisyon", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.pozisyon = :pozisyon"),
    @NamedQuery(name = "IsBilgileri.findBySehir", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.sehir = :sehir"),
    @NamedQuery(name = "IsBilgileri.findByToplamIsTecr\u00fcbe", query = "SELECT \u0131 FROM IsBilgileri \u0131 WHERE \u0131.toplamIsTecr\u00fcbe = :toplamIsTecr\u00fcbe")})
public class IsBilgileri implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "isID")
    private String isID;
    @Basic(optional = false)
    @Column(name = "calisma_durumu")
    private boolean calismaDurumu;
    @Basic(optional = false)
    @Column(name = "firma_adi")
    private String firmaAdi;
    @Basic(optional = false)
    @Column(name = "baslangic_tarihi")
    private String baslangicTarihi;
    @Basic(optional = false)
    @Column(name = "bitis_tarihi")
    private String bitisTarihi;
    @Basic(optional = false)
    @Column(name = "sektor")
    private String sektor;
    @Basic(optional = false)
    @Column(name = "ulke")
    private String ulke;
    @Basic(optional = false)
    @Column(name = "is_alani")
    private String isAlani;
    @Basic(optional = false)
    @Column(name = "pozisyon")
    private String pozisyon;
    @Basic(optional = false)
    @Column(name = "sehir")
    private String sehir;
    @Basic(optional = false)
    @Column(name = "toplam_is_tecr\u00fcbe")
    private int toplamIsTecrübe;
    @OneToMany(mappedBy = "isID")
    private Collection<Kullanici> kullaniciCollection;

    public IsBilgileri() {
    }

    public IsBilgileri(String isID) {
        this.isID = isID;
    }

    public IsBilgileri(String isID, boolean calismaDurumu, String firmaAdi, String baslangicTarihi, String bitisTarihi, String sektor, String ulke, String isAlani, String pozisyon, String sehir, int toplamIsTecrübe) {
        this.isID = isID;
        this.calismaDurumu = calismaDurumu;
        this.firmaAdi = firmaAdi;
        this.baslangicTarihi = baslangicTarihi;
        this.bitisTarihi = bitisTarihi;
        this.sektor = sektor;
        this.ulke = ulke;
        this.isAlani = isAlani;
        this.pozisyon = pozisyon;
        this.sehir = sehir;
        this.toplamIsTecrübe = toplamIsTecrübe;
    }

    public String getIsID() {
        return isID;
    }

    public void setIsID(String isID) {
        this.isID = isID;
    }

    public boolean getCalismaDurumu() {
        return calismaDurumu;
    }

    public void setCalismaDurumu(boolean calismaDurumu) {
        this.calismaDurumu = calismaDurumu;
    }

    public String getFirmaAdi() {
        return firmaAdi;
    }

    public void setFirmaAdi(String firmaAdi) {
        this.firmaAdi = firmaAdi;
    }

    public String getBaslangicTarihi() {
        return baslangicTarihi;
    }

    public void setBaslangicTarihi(String baslangicTarihi) {
        this.baslangicTarihi = baslangicTarihi;
    }

    public String getBitisTarihi() {
        return bitisTarihi;
    }

    public void setBitisTarihi(String bitisTarihi) {
        this.bitisTarihi = bitisTarihi;
    }

    public String getSektor() {
        return sektor;
    }

    public void setSektor(String sektor) {
        this.sektor = sektor;
    }

    public String getUlke() {
        return ulke;
    }

    public void setUlke(String ulke) {
        this.ulke = ulke;
    }

    public String getIsAlani() {
        return isAlani;
    }

    public void setIsAlani(String isAlani) {
        this.isAlani = isAlani;
    }

    public String getPozisyon() {
        return pozisyon;
    }

    public void setPozisyon(String pozisyon) {
        this.pozisyon = pozisyon;
    }

    public String getSehir() {
        return sehir;
    }

    public void setSehir(String sehir) {
        this.sehir = sehir;
    }

    public int getToplamIsTecrübe() {
        return toplamIsTecrübe;
    }

    public void setToplamIsTecrübe(int toplamIsTecrübe) {
        this.toplamIsTecrübe = toplamIsTecrübe;
    }

    @XmlTransient
    public Collection<Kullanici> getKullaniciCollection() {
        return kullaniciCollection;
    }

    public void setKullaniciCollection(Collection<Kullanici> kullaniciCollection) {
        this.kullaniciCollection = kullaniciCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (isID != null ? isID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IsBilgileri)) {
            return false;
        }
        IsBilgileri other = (IsBilgileri) object;
        if ((this.isID == null && other.isID != null) || (this.isID != null && !this.isID.equals(other.isID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "POJOS.IsBilgileri[ isID=" + isID + " ]";
    }
    
}
