/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package POJOS;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gizem
 */
@Entity
@Table(name = "egitim")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Egitim.findAll", query = "SELECT e FROM Egitim e"),
    @NamedQuery(name = "Egitim.findByEgitimID", query = "SELECT e FROM Egitim e WHERE e.egitimID = :egitimID"),
    @NamedQuery(name = "Egitim.findByBaslangicTarihi", query = "SELECT e FROM Egitim e WHERE e.baslangicTarihi = :baslangicTarihi"),
    @NamedQuery(name = "Egitim.findByBitisTarihi", query = "SELECT e FROM Egitim e WHERE e.bitisTarihi = :bitisTarihi"),
    @NamedQuery(name = "Egitim.findByOkulAdi", query = "SELECT e FROM Egitim e WHERE e.okulAdi = :okulAdi"),
    @NamedQuery(name = "Egitim.findByBolumAdi", query = "SELECT e FROM Egitim e WHERE e.bolumAdi = :bolumAdi"),
    @NamedQuery(name = "Egitim.findBySehir", query = "SELECT e FROM Egitim e WHERE e.sehir = :sehir")})
public class Egitim implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "egitimID")
    private String egitimID;
    @Basic(optional = false)
    @Column(name = "baslangic_tarihi")
    private String baslangicTarihi;
    @Basic(optional = false)
    @Column(name = "bitis_tarihi")
    private String bitisTarihi;
    @Basic(optional = false)
    @Column(name = "okul_adi")
    private String okulAdi;
    @Basic(optional = false)
    @Column(name = "bolum_adi")
    private String bolumAdi;
    @Basic(optional = false)
    @Column(name = "sehir")
    private String sehir;
    @OneToMany(mappedBy = "egitimID")
    private Collection<Kullanici> kullaniciCollection;

    public Egitim() {
    }

    public Egitim(String egitimID) {
        this.egitimID = egitimID;
    }

    public Egitim(String egitimID, String baslangicTarihi, String bitisTarihi, String okulAdi, String bolumAdi, String sehir) {
        this.egitimID = egitimID;
        this.baslangicTarihi = baslangicTarihi;
        this.bitisTarihi = bitisTarihi;
        this.okulAdi = okulAdi;
        this.bolumAdi = bolumAdi;
        this.sehir = sehir;
    }

    public String getEgitimID() {
        return egitimID;
    }

    public void setEgitimID(String egitimID) {
        this.egitimID = egitimID;
    }

    public String getBaslangicTarihi() {
        return baslangicTarihi;
    }

    public void setBaslangicTarihi(String baslangicTarihi) {
        this.baslangicTarihi = baslangicTarihi;
    }

    public String getBitisTarihi() {
        return bitisTarihi;
    }

    public void setBitisTarihi(String bitisTarihi) {
        this.bitisTarihi = bitisTarihi;
    }

    public String getOkulAdi() {
        return okulAdi;
    }

    public void setOkulAdi(String okulAdi) {
        this.okulAdi = okulAdi;
    }

    public String getBolumAdi() {
        return bolumAdi;
    }

    public void setBolumAdi(String bolumAdi) {
        this.bolumAdi = bolumAdi;
    }

    public String getSehir() {
        return sehir;
    }

    public void setSehir(String sehir) {
        this.sehir = sehir;
    }

    @XmlTransient
    public Collection<Kullanici> getKullaniciCollection() {
        return kullaniciCollection;
    }

    public void setKullaniciCollection(Collection<Kullanici> kullaniciCollection) {
        this.kullaniciCollection = kullaniciCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (egitimID != null ? egitimID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Egitim)) {
            return false;
        }
        Egitim other = (Egitim) object;
        if ((this.egitimID == null && other.egitimID != null) || (this.egitimID != null && !this.egitimID.equals(other.egitimID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "POJOS.Egitim[ egitimID=" + egitimID + " ]";
    }
    
}
