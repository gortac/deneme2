/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package POJOS;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gizem
 */
@Entity
@Table(name = "iskaydi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Iskaydi.findAll", query = "SELECT \u0131 FROM Iskaydi \u0131"),
    @NamedQuery(name = "Iskaydi.findBySirketAdi", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.sirketAdi = :sirketAdi"),
    @NamedQuery(name = "Iskaydi.findBySirketKodu", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.sirketKodu = :sirketKodu"),
    @NamedQuery(name = "Iskaydi.findByTelefon", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.telefon = :telefon"),
    @NamedQuery(name = "Iskaydi.findByEmail", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.email = :email"),
    @NamedQuery(name = "Iskaydi.findByAdres", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.adres = :adres"),
    @NamedQuery(name = "Iskaydi.findByPozisyonAdi", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.pozisyonAdi = :pozisyonAdi"),
    @NamedQuery(name = "Iskaydi.findByPozisyonTipi", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.pozisyonTipi = :pozisyonTipi"),
    @NamedQuery(name = "Iskaydi.findBySehir", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.sehir = :sehir"),
    @NamedQuery(name = "Iskaydi.findByKrediKart\u0131Numaras\u0131", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.krediKart\u0131Numaras\u0131 = :krediKart\u0131Numaras\u0131"),
    @NamedQuery(name = "Iskaydi.findByG\u00fcvenlikKodu", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.g\u00fcvenlikKodu = :g\u00fcvenlikKodu"),
    @NamedQuery(name = "Iskaydi.findByKartTipi4", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.kartTipi4 = :kartTipi4"),
    @NamedQuery(name = "Iskaydi.findByIsTanimi", query = "SELECT \u0131 FROM Iskaydi \u0131 WHERE \u0131.isTanimi = :isTanimi")})
public class Iskaydi implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sirketAdi")
    private String sirketAdi;
    @Basic(optional = false)
    @Column(name = "sirketKodu")
    private String sirketKodu;
    @Basic(optional = false)
    @Column(name = "telefon")
    private String telefon;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "adres")
    private String adres;
    @Basic(optional = false)
    @Column(name = "pozisyonAdi")
    private String pozisyonAdi;
    @Basic(optional = false)
    @Column(name = "pozisyonTipi")
    private String pozisyonTipi;
    @Basic(optional = false)
    @Column(name = "sehir")
    private String sehir;
    @Column(name = "krediKart\u0131Numaras\u0131")
    private String krediKartıNumarası;
    @Basic(optional = false)
    @Column(name = "g\u00fcvenlikKodu")
    private String güvenlikKodu;
    @Basic(optional = false)
    @Column(name = "kartTipi4")
    private String kartTipi4;
    @Basic(optional = false)
    @Column(name = "isTanimi")
    private String isTanimi;

    public Iskaydi() {
    }

    public Iskaydi(String sirketAdi) {
        this.sirketAdi = sirketAdi;
    }

    public Iskaydi(String sirketAdi, String sirketKodu, String telefon, String email, String adres, String pozisyonAdi, String pozisyonTipi, String sehir, String güvenlikKodu, String kartTipi4, String isTanimi) {
        this.sirketAdi = sirketAdi;
        this.sirketKodu = sirketKodu;
        this.telefon = telefon;
        this.email = email;
        this.adres = adres;
        this.pozisyonAdi = pozisyonAdi;
        this.pozisyonTipi = pozisyonTipi;
        this.sehir = sehir;
        this.güvenlikKodu = güvenlikKodu;
        this.kartTipi4 = kartTipi4;
        this.isTanimi = isTanimi;
    }

    public String getSirketAdi() {
        return sirketAdi;
    }

    public void setSirketAdi(String sirketAdi) {
        this.sirketAdi = sirketAdi;
    }

    public String getSirketKodu() {
        return sirketKodu;
    }

    public void setSirketKodu(String sirketKodu) {
        this.sirketKodu = sirketKodu;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getPozisyonAdi() {
        return pozisyonAdi;
    }

    public void setPozisyonAdi(String pozisyonAdi) {
        this.pozisyonAdi = pozisyonAdi;
    }

    public String getPozisyonTipi() {
        return pozisyonTipi;
    }

    public void setPozisyonTipi(String pozisyonTipi) {
        this.pozisyonTipi = pozisyonTipi;
    }

    public String getSehir() {
        return sehir;
    }

    public void setSehir(String sehir) {
        this.sehir = sehir;
    }

    public String getKrediKartıNumarası() {
        return krediKartıNumarası;
    }

    public void setKrediKartıNumarası(String krediKartıNumarası) {
        this.krediKartıNumarası = krediKartıNumarası;
    }

    public String getGüvenlikKodu() {
        return güvenlikKodu;
    }

    public void setGüvenlikKodu(String güvenlikKodu) {
        this.güvenlikKodu = güvenlikKodu;
    }

    public String getKartTipi4() {
        return kartTipi4;
    }

    public void setKartTipi4(String kartTipi4) {
        this.kartTipi4 = kartTipi4;
    }

    public String getIsTanimi() {
        return isTanimi;
    }

    public void setIsTanimi(String isTanimi) {
        this.isTanimi = isTanimi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sirketAdi != null ? sirketAdi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Iskaydi)) {
            return false;
        }
        Iskaydi other = (Iskaydi) object;
        if ((this.sirketAdi == null && other.sirketAdi != null) || (this.sirketAdi != null && !this.sirketAdi.equals(other.sirketAdi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "POJOS.Iskaydi[ sirketAdi=" + sirketAdi + " ]";
    }
    
}
